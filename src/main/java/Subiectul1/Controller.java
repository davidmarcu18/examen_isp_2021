package Subiectul1;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.io.*;

public class Controller {
@FXML
    TextField nume_fisier;
@FXML
    TextField nr_caractere;
    public void button_action(javafx.event.ActionEvent actionEvent) throws IOException {
        File file = new File(nume_fisier.getText());
        FileInputStream fileStream = new FileInputStream(file);
        InputStreamReader input = new InputStreamReader(fileStream);
        BufferedReader reader = new BufferedReader(input);

        String line;

        int characterCount = 0;

        // Reading line by line from the
        // file until a null is returned
        while((line = reader.readLine()) != null) {
            if (line.equals("")) {
            } else {
                characterCount += line.length();
            }
        }
        nr_caractere.setText(String.valueOf(characterCount));
    }
}

